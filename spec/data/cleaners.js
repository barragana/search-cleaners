export default [
  {
    id: 'dfT9wFR37QemWyyikzEx',
    name: 'Maria',
    country_code: 'de',
    latitude: 52.4826466,
    longitude: 13.3554251,
    gender: 'F',
    preferences: 'fridge,windows,laundry,ironing,oven'
  },
  {
    id: 'NWU13z1PbC32Dg3KroTt',
    name: 'David',
    country_code: 'de',
    latitude: 52.5126466,
    longitude: 13.3654251,
    gender: 'M',
    preferences: 'fridge,windows,laundry,oven'
  },
  {
    id: 'QxGgd09qUZ7BcLWNaOI1',
    name: 'Sara',
    country_code: 'nl',
    latitude: 52.3650172,
    longitude: 4.8375675,
    gender: 'F',
    preferences: 'fridge,windows,laundry'
  }
];
