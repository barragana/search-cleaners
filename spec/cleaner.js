import { expect } from 'chai';
import knex from 'knex';

import cleanersList from './data/cleaners';
import knexConfig from '../config/knex';
import countryConfig from '../config/country';
import { list, find } from '../src/services/cleaner';

describe('cleaner service', function () {
  let pg;
  let tableName;
  let table;

  try {
    before(() => {
      pg = knex(knexConfig);
      tableName = 'cleaners_test';

      return Promise.all([pg.schema.createTableIfNotExists(tableName,
        function (table) {
          table.string('id').primary();
          table.string('name', 255);
          table.string('country_code', 2);
          table.float('latitude');
          table.float('longitude');
          table.string('gender', 1);
          table.string('preferences', 255);
        })
        .then(function () {
          return pg.insert(cleanersList).into(tableName)
          .then(function () {
            return {inserted: true};
          });
        })
      ]);
    });

    after(() => {
      return dropAndDisconect(pg, tableName);
    });

    beforeEach(() => {
      table = pg(tableName);
    });

    afterEach(() => {
      table = null;
    });

    describe('list all', function () {
      it('should be able to list three cleaners', function () {
        return Promise.resolve(
          list(table)
          .then(function (result) {
            expect(result).to.have.length(3);
          })
        );
      });
    });

    describe('find by country "nl" and geo', function () {
      it('should be able to find only one cleaner', function () {
        const query = {
          country_code: 'nl',
          latitude: 52.3650172,
          longitude: 4.8375675,
          distance: countryConfig.nl
        };

        return Promise.resolve(
          find(table, query)
          .then(function (result) {
            expect(result).to.have.length(1);
            expect(result[0].name).to.be.equal('Sara');
          })
        );
      });
    });

    describe('find by country "de" and geo', function () {
      it('should be able to find two cleaners', function () {
        const query = {
          country_code: 'de',
          latitude: 52.5126466,
          longitude: 13.4154251,
          distance: countryConfig.de
        };

        return Promise.resolve(
          find(table, query)
          .then(function (result) {
            expect(result).to.have.length(2);
            expect(result[0].name).to.be.equal('David');
            expect(result[0].gender).to.be.equal('M');
            expect(result[1].name).to.be.equal('Maria');
            expect(result[1].gender).to.be.equal('F');
          })
        );
      });
    });

    describe('find by country "de", geo, gender "M"', function () {
      it('should be able to find one cleaner', function () {
        const query = {
          country_code: 'de',
          latitude: 52.5126466,
          longitude: 13.4154251,
          gender: 'M',
          distance: countryConfig.de
        };

        return Promise.resolve(
          find(table, query)
          .then(function (result) {
            expect(result).to.have.length(1);
            expect(result[0].name).to.be.equal('David');
            expect(result[0].gender).to.be.equal('M');
          })
        );
      });
    });

    describe('find by country "de", geo and preferences "windows, ironing"', function () {
      it('should be able to find one cleaner', function () {
        const query = {
          country_code: 'de',
          latitude: 52.5126466,
          longitude: 13.4154251,
          distance: countryConfig.de,
          preferences: '\'windows\', \'ironing\''
        };

        return Promise.resolve(
          find(table, query)
          .then(function (result) {
            expect(result).to.have.length(1);
            expect(result[0].name).to.be.equal('Maria');
            expect(result[0].gender).to.be.equal('F');
          })
        );
      });
    });

    describe('find by country "de", geo and preferences "windows, oven"', function () {
      it('should be able to find two cleaners', function () {
        const query = {
          country_code: 'de',
          latitude: 52.5126466,
          longitude: 13.4154251,
          distance: countryConfig.de,
          preferences: '\'windows\', \'oven\''
        };

        return Promise.resolve(
          find(table, query)
          .then(function (result) {
            expect(result).to.have.length(2);
            expect(result[0].name).to.be.equal('David');
            expect(result[0].gender).to.be.equal('M');
            expect(result[1].name).to.be.equal('Maria');
            expect(result[1].gender).to.be.equal('F');
          })
        );
      });
    });

    describe('find by country "de", geo, gender "M" and preferences "windows, oven"', function () {
      it('should be able to find one cleaner', function () {
        const query = {
          country_code: 'de',
          latitude: 52.5126466,
          longitude: 13.4154251,
          distance: countryConfig.de,
          gender: 'M',
          preferences: '\'windows\', \'oven\''
        };

        return Promise.resolve(
          find(table, query)
          .then(function (result) {
            expect(result).to.have.length(1);
            expect(result[0].name).to.be.equal('David');
            expect(result[0].gender).to.be.equal('M');
          })
        );
      });
    });

    describe('find by country "nl", geo, gender "F" and preferences "fridge, ironing"', function () {
      it('should be able to find no cleaners', function () {
        const query = {
          country_code: 'nl',
          latitude: 52.3650172,
          longitude: 4.8375675,
          distance: countryConfig.nl,
          gender: 'F',
          preferences: '\'fridge\', \'ironing\''
        };

        return Promise.resolve(
          find(table, query)
          .then(function (result) {
            expect(result).to.have.length(0);
          })
        );
      });
    });
  }
  catch (e) {
    return dropAndDisconect(pg, tableName);
  }
});


function dropAndDisconect (pg, tableName) {
  return pg.schema.dropTable(tableName)
  .then(pg.destroy, pg.destroy);
}
