import { expect } from 'chai';
const minimist = require('minimist');
import knex from 'knex';

import env from '../config/env';

import knexConfig from '../config/knex';
import parser from '../src/util/parser';
import { distanceForPostgis, distanceForPostgres } from '../src/util/queryBuilder';

describe('util', function () {
  const args = [
    '--country=de',
    '--geo=52.5126466,13.4154251',
    '--gender=F',
    '--preferences=windows,oven'
  ];
  const params = minimist(args);
  delete params._;

  describe('parser', function () {
    it('should be able to parse parameters to query', function () {
      const query = parser(params);
      expect(query.country_code).to.be.equal('de');
      expect(query.gender).to.be.equal('F');
      expect(query.latitude).to.be.equal('52.5126466');
      expect(query.longitude).to.be.equal('13.4154251');
      expect(query.preferences).to.be.equal('\'windows\',\'oven\'');
      expect(query.distance).to.be.equal(10);
      return Promise.resolve();
    });
  });

  describe('query builder', function () {
    let pg;

    before(() => {
      pg = knex(knexConfig);
    });

    after(() => {
      pg.destroy();
    });

    it('should be able to get distance between two geo locations for postgres', function () {
      const query = distanceForPostgres(52.5126466, 13.4154251, 52.3650172, 4.8375675);

      return Promise.resolve(
        pg.raw(`select ${query} as distance`)
        .then(result => {
          expect(result.rows[0].distance).to.be.equal(581.342238389442);
        })
      );
    });

    if (env.db.postgis) {
      it('should be able to get distance between two geo locations for postgis', function () {
        const query = distanceForPostgis(52.5126466, 13.4154251, 52.3650172, 4.8375675);

        return Promise.resolve(
          pg.raw(`select ${query} as distance`)
          .then(result => {
            expect(result.rows[0].distance).to.be.equal(581.343038765217);
          })
        );
      });
    }
  });

});
