const fs = require('fs');
const minimist = require('minimist');
const gulp = require('gulp');
const babel = require('gulp-babel');
const eslint = require('gulp-eslint');
const mocha = require('gulp-mocha');
const sequence = require('gulp-sequence');
const gutil = require('gulp-util');

require('babel-core/register');

gulp.task('get-matches', function (done) {
  return sequence('start-' + (process.env.NODE_ENV || 'development'))(done);
});

gulp.task('start-development', function (done) {
  gutil.log(gutil.colors.green('Start development tasks'));
  sequence('build', 'find')(done);
});

gulp.task('start-production', function (done) {
  gutil.log(gutil.colors.green('Start production tasks'));
  if (fileExists('./build/main.js')) {
    gutil.log(gutil.colors.gray('No compilation required'));
    return sequence('find')(done);
  }
  sequence('lint', 'test', 'build', 'find')(done);
});

gulp.task('lint', function () {
  gutil.log(gutil.colors.yellow('Validate code style'));
  return gulp.src(['**/*.js','!node_modules/**', '!build/**'])
  .pipe(eslint())
  .pipe(eslint.format())
  .pipe(eslint.failAfterError());
});

gulp.task('test', function (done) {
  gutil.log(gutil.colors.yellow('Run integration and unit tests'));
  gulp.src(['spec/**/*.js'], { read: false })
  .pipe(mocha({ reporter: 'spec' }))
  .once('error', function (){
    done();
    process.exit(1);
  })
  .once('end', function (){
    return done();
  });
});

gulp.task('build', function () {
  gutil.log(gutil.colors.blue('Compiling ES6 files'));
  return gulp.src('src/**/*.js')
  .pipe(babel({ presets: ['es2015'] }))
  .pipe(gulp.dest('build'));
});

gulp.task('find', function () {
  gutil.log(gutil.colors.green('Start searching cleaners'));
  const params = getParams();

  if (!fileExists('./build/main.js')) {
    gutil.log(gutil.colors.red('Error:'), gutil.colors.yellow('Files were not compiled'));
    return;
  }

  if(!params.country || !params.geo){
    gutil.log(gutil.colors.red('Error:'), gutil.colors.yellow('--country and --geo are required parameters'));
    return;
  }

  return require('./build/main').default(params);
});

gulp.task('default', function (done) {
  sequence('find')(done);
});

function getParams () {
  const params = minimist(process.argv.slice(2));
  delete params._;

  return params;
}

function fileExists (path) {
  return fs.existsSync(path, function (exists) {
    return exists;
  });
}
