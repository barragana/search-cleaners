export default {
  db: {
    host: process.env.DB_HOST || '127.0.0.1',
    port: process.env.DB_PORT || 5432,
    user: process.env.DB_USER || 'bookatiger',
    password: process.env.DB_PASSWORD || 'bookatiger',
    database: process.env.DB_DATABASE || 'bookatiger',
    postgis: process.env.DB_POSTGIS || true
  }
};
