import env from './env';

export default  {
  client: 'pg',
  debug: false,
  connection: {
    host: env.db.host,
    user: env.db.user,
    port: env.db.port,
    password: env.db.password,
    database: env.db.database
  },
  searchPath: '"$user",public'
};
