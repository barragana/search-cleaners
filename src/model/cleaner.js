import { find } from '../services/cleaner';

export default {
  getMatches: (table, query) => {
    return find (table, query)
    .then(cleaners => {
      displayMatches(cleaners);
    });
  }
};

function displayMatches (cleaners) {
  if (!cleaners.length) {
    return console.log('No matches found.');
  }
  for (const cleaner of cleaners) {
    console.log(prepare(cleaner));
  }
}

function prepare (cleaner) {
  return Object.keys(cleaner).reduce((value, key) => {
    return value + `- ${key}: ${cleaner[key]} `;
  }, '');
}
