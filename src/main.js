import knex from 'knex';

import knexConfig from '../config/knex';
import parser from './util/parser';
import cleaner from './model/cleaner';

export default (params) => {
  const query = parser(params);
  const pg = knex(knexConfig);
  const table = pg('cleaners');

  return cleaner.getMatches(table, query)
  .then(pg.destroy, pg.destroy)
  .catch(err => {
    console.error(err.message);
  });
};
