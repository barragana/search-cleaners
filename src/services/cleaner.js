import queryBuilder from '../util/queryBuilder';

export function list (db) {
  return db.select(['id', 'name', 'gender', 'preferences'])
  .then(cleaners => { return cleaners; });
}

export function find (db, params) {
  const distance = queryBuilder.distance(params.latitude, params.longitude, 'latitude', 'longitude');

  const query = db.select(['id', 'name', 'gender', 'preferences'])
    .where('country_code',  params.country_code)
    .whereRaw(`${distance} <= ${params.distance}`);

  if (params.gender) {
    query.andWhere('gender', params.gender);
  }

  if (params.preferences) {
    query.whereRaw(`regexp_split_to_array(preferences, ',') @> ARRAY[${params.preferences}]`);
  }

  return query.orderByRaw(`${distance}`);
}
