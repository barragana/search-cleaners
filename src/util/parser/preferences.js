export default (params) => {
  return {
    preferences: params.split(',').reduce((value, pref, i) => {
      if (i != 0) {
        value = value + ',';
      }
      return value + `\'${pref}\'`;
    }, '')
  };
};
