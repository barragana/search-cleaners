export default (params) => {
  const geo = params.split(',');
  return {
    latitude: geo[0],
    longitude: geo[1]
  };
};
