import fs from 'fs';
import _ from 'lodash';

import countryConfig from '../../../config/country';

export default (params) => {

  let parsers = {};

  fs.readdirSync(__dirname)
   .filter((file) => (file !== 'index.js'))
   .forEach((file) => {
     parsers[file.split('.js')[0]] = require(`./${file}`).default;
   });

  const query = Object.keys(params).reduce((value, key) => {
    value = _.merge(parsers[key](params[key]), value);
    return value;
  }, {});

  query.distance = countryConfig[query.country_code];
  return query;
};
