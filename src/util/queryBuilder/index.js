import env from '../../../config/env';

export default {
  distance: (lat1, lon1, lat2, lon2) => {
    if (env.db.postgis) {
      return distanceForPostgis(lat1, lon1, lat2, lon2);
    }

    return distanceForPostgres(lat1, lon1, lat2, lon2);
  }
};

export function distanceForPostgres (lat1, lon1, lat2, lon2) {
  return `(6371*2*atan2(
      (|/((sin((radians(${lat1})-radians(${lat2}))/2))^2 + cos(radians(${lat2})) * cos(radians(${lat1})) * (sin((radians(${lon1})-radians(${lon2}))/2))^2)),
   (|/(1-((sin((radians(${lat1})-radians(${lat2}))/2))^2 + cos(radians(${lat2})) * cos(radians(${lat1})) * (sin((radians(${lon1})-radians(${lon2}))/2))^2)))
  ))`;
}

export function distanceForPostgis (lat1, lon1, lat2, lon2) {
  return `st_distance_sphere(
     st_geomfromtext('POINT(${lon1} ${lat1})', 4326),
     st_geomfromtext('POINT(' || ${lon2} || ' ' || ${lat2} || ')', 4326)
   )/1000`;
}
