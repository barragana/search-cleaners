#search-cleaners
----------------
Search for cleaners by geo location(latitude and longitude) in a certain radius distance according to the country.

##1. Prepare Environment
----------------------
1. **Install postgresql**
    1. configure user and password
2. **Install postgis** _(Optional)_
3. **Create database**
    1. Grant all roles and privileges to your user
    2. Add postgis extension, **in case you had installed**
    3. Create table _**cleaners**_ with the following columns
        * **id**: varchar(20), primary key
        * **name**: varchar(255), not null
        * **country_code**: varchar(2), not null
        * **latitude**: float, not null
        * **longitude**: float, not null
        * **gender**: char
        * **preferences**: varchar(255)
    4. Insert some cleaners to the database
4. **Export your postgres configuration to the folowing environment variable**
    1. DB\_HOST - _ex:export DB\_HOST=127.0.0.1_
    2. DB\_PORT - _ex:export DB\_PORT=5432_
    3. DB\_USER
    4. DB\_PASSWORD
    5. DB\_DATABASE
    6. DB\_POSTGIS = true - **_(Only if you installed and add postgis to your database)_**
    7. NODE\_ENV - (_production_) or (_development_)
5. **Clone project**
6. **Into root folder run _"npm install"_**

##2. Run
---------
* **gulp get-matches <params>**
    * params _(**gender** and **preferences** are optional)_
        - `--country=<country_acronym>`
        - `--geo=<latitude>,<longitude>`
        - `--gender=M/F`
        - `--preferences=<p1>,<p2>,<p3>`
* **Exaples of command to run**
    - `gulp get-matches --country=de --geo=52.5126466,13.4154251`
    - `gulp get-matches --country=de --geo=52.5126466,13.4154251 --gender=M`
    - `gulp get-matches --country=de --geo=52.5126466,13.4154251 --gender=F --preferences=fridge,windows,ironing`

##Addtional Information
-----------------------
####**Gulp tasks**
* Gulp **lint**
    * Validate if the code style is according to defined code style pattern configuration
* Gulp **test**
    * Run integration test with database _(Obs: no connection testing)_
    * Run unit tests
* Gulp **build**
    * Compile ES6 files
* Gulp **find** --country=<country_acronym> --geo=<latitude>,<longitude> --gender=M/F --preferences=<p1>,<p2>,<p3>
    * Find cleaners _(Obs: --gender and --preferences are optional)_
* Gulp **start-development**
    * Run in sequence _build_ and _find_
* Gulp **start-production**
    * (First time) Once files are not built yet, it runs in sequence _lint_, _test_, _build_, _find_
    * (Once files are built) it runs _find_